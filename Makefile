TARGET := youtube-colors.user.css

JINJA2 := jinja2

TEMPLATE := youtube-colors.css
COLORS := colors.yml

main: $(TARGET)

$(TARGET): $(COLORS) $(TEMPLATE)
	$(JINJA2) $(TEMPLATE) $(COLORS) > $(TARGET)
