# YouTube Colors

Custom colors for [YouTube]. This is a userstyle version of the [style by void][original-style] on
[userstyles.org].

To install the latest, [click here][latest-style].

[latest-style]: https://gitlab.com/saneki/youtube-colors/raw/master/youtube-colors.user.css
[original-style]: https://userstyles.org/styles/95280/youtube-custom-colors-video-progress-bar
[userstyles.org]: https://userstyles.org
[YouTube]: https://www.youtube.com
